/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

/**
 * @file  example01.cpp
 * @brief Demonstrates the basic usage of the FwdDiff class to compute
 *        derivatives
 */

#include <iostream>
#include <iomanip>
#include "FwdDiff/FwdDiff.hpp"

/**
 * \brief Sample function y = x*sin(x^2) + x
 *
 * The function has been templated for a generic type T to enable differentiation
 */
template <typename T>
T f(const T& x)
{
  return x*sin(pow(x, 2.0)) + x;
}

/**
 * \brief Analytical derivative of sample function y = x*sin(x^2) + x
 */
double dfdx(const double& x)
{
  return sin(pow(x, 2.0)) + x*cos(pow(x, 2.0))*2.0*x + 1.0;
}

/**
 * \brief Function to print horizontal rule of given width
 */
void printHorizontalRule(size_t width)
{
  std::cout << " +" << std::setfill('-') << std::setw(width-2) << "+\n";
}

/**
 * \brief Function to print a row with four elements
 */
template <typename T>
void printLine(size_t width, T x0, T x1, T x2, T x3)
{
  int w = (width-13)/4;
  std::cout << std::setfill(' ');
  std::cout << " | " << std::setw(w) << x0;
  std::cout << " | " << std::setw(w) << x1;
  std::cout << " | " << std::setw(w) << x2;
  std::cout << " | " << std::setw(w) << x3;
  std::cout << " |\n";
}

/**
 * \brief Main function
 *
 * This function compares analytical derivatives with those obtained by
 * automatic differentiation
 */
int main()
{
  // Create a variable of type FwdDiff
  FwdDiff<double> x;

  // Set the number of test points and the range
  int n = 10;
  double range = 2.0;

  // Print header
  int width = 67;
  printHorizontalRule(width);
  printLine(width, "x", "f(x)", "df/dx [exact]", "df/dx [auto]");
  printHorizontalRule(width);

  // Loop through the test points
  for (int i = 0; i <= n; ++i)
  {
    // Compute x value
    double val = double(i)*(range/double(n));
    x.setVal(val);
    x.setDx(1.0);

    // Evaluate function
    FwdDiff<double> y = f(x);

    // Print values and derivatives
    printLine(67, val, f(val), dfdx(val), y.dx(0));
  }

  printHorizontalRule(width);

  return 0;
}
