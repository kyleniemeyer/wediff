/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#ifndef _FWD_DIFF_MANAGER_HPP_
#define _FWD_DIFF_MANAGER_HPP_

/**
 * @file  FwdDiffManager.hpp
 * @brief Contains FwdDiffManager class definition
 */

/**
 * @class FwdDiffManager
 * @brief Class used to define the configuration of FwdDiff objects.
 */
class FwdDiffManager
{
public:

  /** Default constructor */
  FwdDiffManager() { }

  /** Function to set number of derivative components */
  void setNumComponents(const unsigned int numComponents)
  {
    _numComponents = numComponents;
  }

  /** Function to get number of derivative components */
  unsigned int getNumComponents()
  {
    return _numComponents;
  }

  /** Function to reset object to default state */
  void reset()
  {
    _numComponents = 1;
  }

private:

  /** Number of derivative components */
  static unsigned int _numComponents;

};

#endif
