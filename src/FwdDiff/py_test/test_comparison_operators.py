"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff

class TestComparisonOperators(object):
    """Tests comparison operators"""
    
    def test_less_than(self):
        """Test less than operator"""
        a = FwdDiff(0.0, [1.0])
        b = FwdDiff(1.0, [2.0])

        assert a < b

    def test_less_than_or_equal(self):
        """Test less than or equal operator"""
        a = FwdDiff(1.0, [1.0])
        b = FwdDiff(1.0, [2.0])

        assert a <= b

    def test_greater_than(self):
        """Test greater than operator"""
        a = FwdDiff(0.0, [1.0])
        b = FwdDiff(1.0, [2.0])

        assert b > a

    def test_greater_than_or_equal(self):
        """Test greater than or equal operator"""
        a = FwdDiff(1.0, [1.0])
        b = FwdDiff(1.0, [2.0])

        assert a >= b
