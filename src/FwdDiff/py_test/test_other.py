"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff, abs

class TestOther(object):
    """Tests other miscellaneous functions"""
    
    def test_abs(self):
        """Test absolute value function"""
        a = FwdDiff(1.0, [2.0])
        
        b = abs(a)
        
        assert b.val() == 1.0
        assert b.dx(0) == 2.0
        
        a = FwdDiff(-1.0, [2.0])
        
        b = abs(a)
        
        assert b.val() == 1.0
        assert b.dx(0) == -2.0
