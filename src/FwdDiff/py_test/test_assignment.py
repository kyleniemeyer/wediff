"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff

class TestAssignment(object):
    """Tests assignment operators"""
    
    def test_addition_assignment_scalar(self):
        """Test addition-assignment operator with a scalar"""
        a = FwdDiff(1.0, [2.0])
        a += 1.0
        assert a.val() == 2.0
        assert a.dx(0) == 2.0 
        
    def test_addition_assignment_FwdDiff(self):
        """Test addition-assignment operator with a FwdDiff type"""
        a = FwdDiff(1.0, [2.0])
        b = FwdDiff(3.0, [4.0])
        a += b
        assert a.val() == 4.0
        assert a.dx(0) == 6.0
        
    def test_subtraction_assignment_scalar(self):
        """Test subtraction-assignment operator with a scalar"""
        a = FwdDiff(1.0, [2.0])
        a -= 1.0
        assert a.val() == 0.0
        assert a.dx(0) == 2.0 
        
    def test_subtraction_assignment_FwdDiff(self):
        """Test subtraction-assignment operator with a FwdDiff type"""
        a = FwdDiff(1.0, [2.0])
        b = FwdDiff(3.0, [4.0])
        a -= b
        assert a.val() == -2.0
        assert a.dx(0) == -2.0
        
    def test_multiplication_assignment_scalar(self):
        """Test multiplication-assignment operator with a scalar"""
        a = FwdDiff(1.0, [2.0])
        a *= 2.0
        assert a.val() == 2.0
        assert a.dx(0) == 4.0 
        
    def test_multiplication_assignment_FwdDiff(self):
        """Test multiplication-assignment operator with a FwdDiff type"""
        a = FwdDiff(1.0, [2.0])
        b = FwdDiff(3.0, [4.0])
        a *= b
        assert a.val() == 3.0
        assert a.dx(0) == 10.0
        
    def test_division_assignment_scalar(self):
        """Test division-assignment operator with a scalar"""
        a = FwdDiff(1.0, [2.0])
        a /= 2.0
        assert a.val() == 0.5
        assert a.dx(0) == 1.0 
        
    def test_division_assignment_FwdDiff(self):
        """Test division-assignment operator with a FwdDiff type"""
        a = FwdDiff(1.0, [2.0])
        b = FwdDiff(3.0, [4.0])
        a /= b
        assert a.val() == 1.0/3.0
        assert a.dx(0) == 2.0/9.0
        