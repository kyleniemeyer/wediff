"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff import FwdDiffManager
from wediff.double import FwdDiff

class TestConstructors(object):
    """Tests constructors"""
    
    def test_default_constructor(self):
        """Test default constructor"""
        a = FwdDiff()

        assert a.val() == 0.0
        assert a.dx(0) == 0.0

    def test_value_constructor(self):
        """Test constructor from value"""
        a = FwdDiff(2.0)

        assert a.val() == 2.0
        assert a.dx(0) == 0.0

    def test_component_value_constructor(self):
        """Test constructor from component values"""
        a = FwdDiff(1.0, [2.0])

        assert a.val() == 1.0
        assert a.dx(0) == 2.0

        FwdDiffManager().setNumComponents(2)
        a = FwdDiff(1.0, [2.0, 3.0])

        assert a.val() == 1.0
        assert a.dx(0) == 2.0
        assert a.dx(1) == 3.0

        FwdDiffManager().setNumComponents(3)
        a = FwdDiff(1.0, [2.0, 3.0, 4.0])

        assert a.val() == 1.0
        assert a.dx(0) == 2.0
        assert a.dx(1) == 3.0
        assert a.dx(2) == 4.0

        FwdDiffManager().reset()

    def test_copy_constructor(self):
        """Test copy constructor"""
        a = FwdDiff(1.0, [2.0])
        b = FwdDiff(a)
        
        assert a.val() == b.val()
        assert a.dx(0) == b.dx(0)
