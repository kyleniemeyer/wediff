"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff
from wediff.double import math
import math as double_math

class TestPower(object):
    """Tests power functions"""
    
    def test_power(self):
        """Test power function with FwdDiff types"""
        a = FwdDiff(2.0)
        b = FwdDiff(2.0, [1.0])
        c = math.pow(a, b)
                
        assert c.val() == double_math.pow(a.val(), b.val())
        assert c.dx(0) == 4.0*double_math.log(2.0)

        a = FwdDiff(2.0, [1.0])
        b = FwdDiff(2.0)
        c = math.pow(a, b)
                
        assert c.val() == double_math.pow(a.val(), b.val())
        assert c.dx(0) == 4.0
        
        a = FwdDiff(2.0, [1.0])
        b = FwdDiff(2.0, [1.0])
        c = math.pow(a, b)
                
        assert c.val() == double_math.pow(a.val(), b.val())
        assert c.dx(0) == 4.0 + 4.0*double_math.log(2.0)
        
    def test_power_with_constants(self):
        """Test power function with floating point types"""
        a = 2.0
        b = FwdDiff(2.0, [1.0])
        
        c = math.pow(a, b)
        assert c.val() == double_math.pow(a, b.val())
        assert c.dx(0) == 4.0*double_math.log(2.0)
        
        c = math.pow(b, a)
        assert c.val() == double_math.pow(a, b.val())
        assert c.dx(0) == 4.0
        
    def test_square_root(self):
        """Test square root function"""
        a = FwdDiff(4.0, [1.0])
        b = math.sqrt(a)
        
        assert b.val() == 2.0
        assert b.dx(0) == 0.25
        
    def test_cube_root(self):
        """Test cube root function"""
        a = FwdDiff(8.0, [1.0])
        b = math.cbrt(a)
        
        assert b.val() == 2.0
        assert b.dx(0) == double_math.pow(8.0, -2.0/3.0)/3.0
        
    def test_hypot(self):
        """Test hypoteneuse function"""
        a = FwdDiff(2.0, [1.0])
        b = FwdDiff(2.0)
        c = math.hypot(a, b)
        
        assert c.val() == double_math.hypot(a.val(), b.val())
        assert c.dx(0) == a.val()/double_math.hypot(a.val(), b.val())