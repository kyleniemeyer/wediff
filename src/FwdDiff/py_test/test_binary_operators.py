"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff

class TestBinaryOperators(object):
    """Tests binary operators"""
    
    def test_addition(self):
        """Test addition operator"""
        b = FwdDiff(1.0, [2.0])

        a = b + 2
        assert a.val() == 3.0
        assert a.dx(0) == 2.0

        a = 3 + b
        assert a.val() == 4.0
        assert a.dx(0) == 2.0

        c = FwdDiff(2.0, [3.0])

        a = b + c
        assert a.val() == 3.0
        assert a.dx(0) == 5.0

    def test_subtraction(self):
        """Test subtraction operator"""
        b = FwdDiff(1.0, [2.0])

        a = b - 2
        assert a.val() == -1.0
        assert a.dx(0) == 2.0

        a = 3 - b
        assert a.val() == 2.0
        assert a.dx(0) == -2.0

        c = FwdDiff(2.0, [3.0])

        a = b - c
        assert a.val() == -1.0
        assert a.dx(0) == -1.0

    def test_multiplication(self):
        """Test multiplication operator"""
        b = FwdDiff(1.0, [2.0])

        a = b*2
        assert a.val() == 2.0
        assert a.dx(0) == 4.0

        a = 3*b
        assert a.val() == 3.0
        assert a.dx(0) == 6.0

        c = FwdDiff(2.0, [3.0])

        a = b*c
        assert a.val() == 2.0
        assert a.dx(0) == 7.0

    def test_division(self):
        """Test division operator"""
        b = FwdDiff(1.0, [2.0])

        a = b/2
        assert a.val() == 0.5
        assert a.dx(0) == 1.0

        a = 3/b
        assert a.val() == 3.0
        assert a.dx(0) == -6.0

        c = FwdDiff(2.0, [3.0])

        a = b/c
        assert a.val() == 0.5
        assert a.dx(0) == 0.25
