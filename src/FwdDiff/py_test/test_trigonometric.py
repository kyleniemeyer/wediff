"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

import pytest
from wediff.double import FwdDiff
from wediff.double import math
import math as double_math

class TestTrigonometric(object):
    """Tests trigonometric functions"""
    
    def test_sin(self):
        """Test sine function"""
        a = FwdDiff(2.0, [1.0])
        b = math.sin(a)
                
        assert b.val() == double_math.sin(a.val())
        assert b.dx(0) == double_math.cos(a.val())
        
    def test_cos(self):
        """Test cosine function"""
        a = FwdDiff(2.0, [1.0])
        b = math.cos(a)
                
        assert b.val() == double_math.cos(a.val())
        assert b.dx(0) == -double_math.sin(a.val())
        
    def test_tan(self):
        """Test tangent function"""
        a = FwdDiff(2.0, [1.0])
        b = math.tan(a)
                
        assert b.val() == double_math.tan(a.val())
        assert b.dx(0) == 1.0/double_math.pow(double_math.cos(a.val()),2.0)
        
    def test_asin(self):
        """Test asin function"""
        a = FwdDiff(0.5, [1.0])
        b = math.asin(a)
                
        assert b.val() == double_math.asin(a.val())
        assert b.dx(0) == 1.0/double_math.sqrt(1.0 - a.val()*a.val())
        
    def test_acos(self):
        """Test acos function"""
        a = FwdDiff(0.5, [1.0])
        b = math.acos(a)
                
        assert b.val() == double_math.acos(a.val())
        assert b.dx(0) == -1.0/double_math.sqrt(1.0 - a.val()*a.val())
        
    def test_atan(self):
        """Test atan function"""
        a = FwdDiff(0.5, [1.0])
        b = math.atan(a)
                
        assert b.val() == double_math.atan(a.val())
        assert b.dx(0) == 1.0/(1.0 + a.val()*a.val())
        
    def test_atan2(self):
        """Test atan2 function"""
        a = FwdDiff(0.5, [1.0])
        b = FwdDiff(2.0)
        c = math.atan2(a, b)
                
        assert c.val() == double_math.atan2(a.val(), b.val())
        assert c.dx(0) == 1.0/b.val()/(1.0 + double_math.pow(a.val()/b.val(),2.0))
        