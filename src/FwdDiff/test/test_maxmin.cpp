/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(MaxMinFunctions)

/**
 * Test maximum functions
 */
BOOST_AUTO_TEST_CASE(testMaximum)
{
  FwdDiff<double> a;
  FwdDiff<double> b;
  FwdDiff<double> c;

  a.setVal(1.0);
  a.setDx(2.0);
  b.setVal(3.0);
  b.setDx(4.0);

  c = max(a, b);

  BOOST_CHECK_CLOSE(c.val(), 3.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 4.0, SMALL);

  c = fmax(a, b);

  BOOST_CHECK_CLOSE(c.val(), 3.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 4.0, SMALL);
}

/**
 * Test minimum functions
 */
BOOST_AUTO_TEST_CASE(testMinimum)
{
  FwdDiff<double> a;
  FwdDiff<double> b;
  FwdDiff<double> c;

  a.setVal(1.0);
  a.setDx(2.0);
  b.setVal(3.0);
  b.setDx(4.0);

  c = min(a, b);

  BOOST_CHECK_CLOSE(c.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 2.0, SMALL);

  c = fmin(a, b);

  BOOST_CHECK_CLOSE(c.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 2.0, SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
