/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(ExponentialAndLogaritmicFunctions)

/**
 * Test exponential function
 */
BOOST_AUTO_TEST_CASE(testExponential)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = exp(b);

  BOOST_CHECK_CLOSE(a.val(), exp(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), exp(2.0), SMALL);
}

BOOST_AUTO_TEST_CASE(testExponentialMinusOne)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = expm1(b);

  BOOST_CHECK_CLOSE(a.val(), expm1(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), exp(2.0), SMALL);
}

/**
 * Test natural logarithm function
 */
BOOST_AUTO_TEST_CASE(testLog)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = log(b);

  BOOST_CHECK_CLOSE(a.val(), log(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.5, SMALL);
}

/**
* Test base 10 logarithm function
*/
BOOST_AUTO_TEST_CASE(testLog10)
{
 FwdDiff<double> a;
 FwdDiff<double> b;

 b.setVal(2.0);
 b.setDx(1.0);

 a = log10(b);

 BOOST_CHECK_CLOSE(a.val(), log10(2.0), SMALL);
 BOOST_CHECK_CLOSE(a.dx(0), log10(exp(1.0))/2.0, SMALL);
}

/**
 * Test natural logarithm plus one function
 */
BOOST_AUTO_TEST_CASE(testLogPlusOne)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = log1p(b);

  BOOST_CHECK_CLOSE(a.val(), log1p(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 1.0/3.0, SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
