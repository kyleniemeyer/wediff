/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(PowerFunctions)

/**
 * Test power function
 */
BOOST_AUTO_TEST_CASE(testPower)
{
  FwdDiff<double> a;
  FwdDiff<double> b;
  FwdDiff<double> c;

  // Case 1: a = const
  a.setVal(2.0);
  a.setDx(0.0);
  b.setVal(2.0);
  b.setDx(1.0);

  c = pow(a, b);

  BOOST_CHECK_CLOSE(c.val(), 4.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 4.0*log(2.0), SMALL);

  // Case 2: b = const
  a.setVal(2.0);
  a.setDx(1.0);
  b.setVal(2.0);
  b.setDx(0.0);

  c = pow(a, b);

  BOOST_CHECK_CLOSE(c.val(), 4.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 4.0, SMALL);

  // Case 3: a and b both variables
  a.setVal(2.0);
  a.setDx(1.0);
  b.setVal(2.0);
  b.setDx(1.0);

  c = pow(a, b);

  BOOST_CHECK_CLOSE(c.val(), 4.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 4.0 + 4.0*log(2.0), SMALL);
}

/**
 * Test power function with constant first argument
 */
BOOST_AUTO_TEST_CASE(testPowerFirstArgumentConstant)
{
  double a = 2.0;
  FwdDiff<double> b;
  FwdDiff<double> c;

  b.setVal(2.0);
  b.setDx(1.0);

  c = pow(a, b);

  BOOST_CHECK_CLOSE(c.val(), 4.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 4.0*log(2.0), SMALL);
}

/**
 * Test power function with constant second argument
 */
BOOST_AUTO_TEST_CASE(testPowerSecondArgumentConstant)
{
  FwdDiff<double> a;
  double b = 2.0;
  FwdDiff<double> c;

  a.setVal(2.0);
  a.setDx(1.0);

  c = pow(a, b);

  BOOST_CHECK_CLOSE(c.val(), 4.0, SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 4.0, SMALL);
}

/**
 * Test square root function
 */
BOOST_AUTO_TEST_CASE(testSquareRoot)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  a.setVal(4.0);
  a.setDx(1.0);

  b = sqrt(a);

  BOOST_CHECK_CLOSE(b.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(b.dx(0), 0.25, SMALL);
}

/**
 * Test cube root function
 */
BOOST_AUTO_TEST_CASE(testCubeRoot)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  a.setVal(8.0);
  a.setDx(1.0);

  b = cbrt(a);

  BOOST_CHECK_CLOSE(b.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(b.dx(0), pow(8.0, -2.0/3.0)/3.0, SMALL);
}

/**
 * Test hypotenuse function
 */
BOOST_AUTO_TEST_CASE(testHypotenuse)
{
  FwdDiff<double> a;
  FwdDiff<double> b;
  FwdDiff<double> c;

  a.setVal(2.0);
  a.setDx(1.0);
  b.setVal(2.0);

  c = hypot(a, b);

  BOOST_CHECK_CLOSE(c.val(), sqrt(8.0), SMALL);
  BOOST_CHECK_CLOSE(c.dx(0), 2.0/sqrt(8.0), SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
