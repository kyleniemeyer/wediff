/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(Constructors)

/**
 * Test default constructor
 */
BOOST_AUTO_TEST_CASE(testDefaultConstructor)
{
  FwdDiff<double> a;
  BOOST_CHECK_CLOSE(a.val(), 0.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.0, SMALL);
}

/**
 * Test constructor from value
 */
BOOST_AUTO_TEST_CASE(testValueConstructor)
{
  FwdDiff<double> a(2.0);
  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.0, SMALL);
}

/**
 * Test constructor from value with two derivative components
 */
BOOST_AUTO_TEST_CASE(testValueConstructorWithTwoComponents)
{
  FwdDiffManager().setNumComponents(2);
  FwdDiff<double> a(2.0);
  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(1), 0.0, SMALL);
  FwdDiffManager().reset();
}

/**
 * Test constructor from components
 */
BOOST_AUTO_TEST_CASE(testComponentConstructor)
{
  FwdDiffManager().setNumComponents(2);
  FwdDiff<double>::Vector dx(2);
  dx[0] = 2.0;
  dx[1] = 3.0;
  FwdDiff<double> a(1.0, dx);

  BOOST_CHECK_CLOSE(a.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(1), 3.0, SMALL);
  FwdDiffManager().reset();
}

/**
 * Test copy constructor
 */
BOOST_AUTO_TEST_CASE(testCopyConstructor)
{
  FwdDiff<double> a;
  a.setVal(1.0);
  a.setDx(2.0);
  FwdDiff<double> b(a);

  BOOST_CHECK_CLOSE(a.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);
  BOOST_CHECK_CLOSE(b.val(), 1.0, SMALL);
  BOOST_CHECK_CLOSE(b.dx(0), 2.0, SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
