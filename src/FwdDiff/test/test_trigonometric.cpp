/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(TrigonometricFunctions)

/**
 * Test sine function
 */
BOOST_AUTO_TEST_CASE(testSine)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = sin(b);

  BOOST_CHECK_CLOSE(a.val(), sin(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), cos(2.0), SMALL);
}

/**
 * Test cosine function
 */
BOOST_AUTO_TEST_CASE(testCosine)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = cos(b);

  BOOST_CHECK_CLOSE(a.val(), cos(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), -sin(2.0), SMALL);
}

/**
 * Test tangent function
 */
BOOST_AUTO_TEST_CASE(testTangent)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(2.0);
  b.setDx(1.0);

  a = tan(b);

  BOOST_CHECK_CLOSE(a.val(), tan(2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 1.0/pow(cos(2.0),2.0), SMALL);
}

/**
 * Test arcsine function
 */
BOOST_AUTO_TEST_CASE(testArcsine)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(0.5);
  b.setDx(1.0);

  a = asin(b);

  BOOST_CHECK_CLOSE(a.val(), asin(0.5), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 1.0/sqrt(0.75), SMALL);
}

/**
 * Test arccos function
 */
BOOST_AUTO_TEST_CASE(testArccos)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(0.5);
  b.setDx(1.0);

  a = acos(b);

  BOOST_CHECK_CLOSE(a.val(), acos(0.5), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), -1.0/sqrt(0.75), SMALL);
}

/**
 * Test arctan function
 */
BOOST_AUTO_TEST_CASE(testArctan)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(0.5);
  b.setDx(1.0);

  a = atan(b);

  BOOST_CHECK_CLOSE(a.val(), atan(0.5), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.8, SMALL);
}

/**
 * Test arctan2 function
 */
BOOST_AUTO_TEST_CASE(testArctan2)
{
  FwdDiff<double> a;
  FwdDiff<double> b;
  FwdDiff<double> c;

  b.setVal(0.5);
  b.setDx(1.0);
  c.setVal(2.0);

  a = atan2(b, c);

  BOOST_CHECK_CLOSE(a.val(), atan2(0.5, 2.0), SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.470588235294118, SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
