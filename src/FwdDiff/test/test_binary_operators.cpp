/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN

#include <boost/test/unit_test.hpp>

#include "FwdDiff/FwdDiff.hpp"

// Define a tolerance for floating point comparisons
#define SMALL 1e-8

BOOST_AUTO_TEST_SUITE(BinaryOperators)

/**
 * Test addition operators
 */
BOOST_AUTO_TEST_CASE(testAddition)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(1.0);
  b.setDx(2.0);

  a = b + 2.0;

  BOOST_CHECK_CLOSE(a.val(), 3.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);

  a = 3.0 + b;

  BOOST_CHECK_CLOSE(a.val(), 4.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);

  FwdDiff<double> c;

  c.setVal(2.0);
  c.setDx(3.0);

  a = b + c;

  BOOST_CHECK_CLOSE(a.val(), 3.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 5.0, SMALL);
}

/**
 * Test subtraction operators
 */
BOOST_AUTO_TEST_CASE(testSubtraction)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(1.0);
  b.setDx(2.0);

  a = b - 2.0;

  BOOST_CHECK_CLOSE(a.val(), -1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 2.0, SMALL);

  a = 3.0 - b;

  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), -2.0, SMALL);

  FwdDiff<double> c;

  c.setVal(2.0);
  c.setDx(3.0);

  a = b - c;

  BOOST_CHECK_CLOSE(a.val(), -1.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), -1.0, SMALL);
}

/**
 * Test multiplication operators
 */
BOOST_AUTO_TEST_CASE(testMultiplication)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(1.0);
  b.setDx(2.0);

  a = b*2.0;

  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 4.0, SMALL);

  a = 3.0*b;

  BOOST_CHECK_CLOSE(a.val(), 3.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 6.0, SMALL);

  FwdDiff<double> c;

  c.setVal(2.0);
  c.setDx(3.0);
  a = b*c;

  BOOST_CHECK_CLOSE(a.val(), 2.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 7.0, SMALL);
}

/**
 * Test division operators
 */
BOOST_AUTO_TEST_CASE(testDivision)
{
  FwdDiff<double> a;
  FwdDiff<double> b;

  b.setVal(1.0);
  b.setDx(2.0);

  a = b/2.0;

  BOOST_CHECK_CLOSE(a.val(), 0.5, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 1.0, SMALL);

  a = 3.0/b;

  BOOST_CHECK_CLOSE(a.val(), 3.0, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), -6.0, SMALL);

  FwdDiff<double> c;

  c.setVal(2.0);
  c.setDx(3.0);

  a = b/c;

  BOOST_CHECK_CLOSE(a.val(), 0.5, SMALL);
  BOOST_CHECK_CLOSE(a.dx(0), 0.25, SMALL);
}

BOOST_AUTO_TEST_SUITE_END()
