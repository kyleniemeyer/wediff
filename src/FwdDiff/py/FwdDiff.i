/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

%include <std_string.i>
%include <std_vector.i>

%naturalvar;

%module fwd_diff
%{
#include "FwdDiff/FwdDiff.hpp"
#include "FwdDiff/FwdDiff_Inline.hpp"
#include "FwdDiff/FwdDiffManager.hpp"
%}

// Ignore constructors not used in Python
%ignore FwdDiff::FwdDiff(const T& x, const Vector& dx);

// Ignore operators that need to be manually defined
%ignore FwdDiff::operator=;

// Ignore function not used in Python
%ignore FwdDiff::dx() const;

// Instantiate std::vector types that are used
%template() std::vector<double>;

// Include headers
%include "FwdDiff/FwdDiff.hpp"
%include "FwdDiff/FwdDiff_Inline.hpp"
%include "FwdDiff/FwdDiffManager.hpp"

// Implement copy constructor
%extend FwdDiff
{
  FwdDiff(const FwdDiff<T>& x)
  {
    return new FwdDiff<T>(x);
  }
}

// Constructor from value and Python list (std::vector in c++)
%extend FwdDiff
{
  FwdDiff(const T& x, const std::vector<T>& dx) 
  {
    assert(FwdDiffManager().getNumComponents() == dx.size());
    FwdDiff<T>* y = new FwdDiff<T>();
    (*y).setVal(x);
    for (size_t i = 0; i != dx.size(); ++i)
    {
      (*y).setDx(dx[i], i);
    }
    return y;
  }
}

// Print operator
%extend FwdDiff
{
  std::string __str__() const
  {
    std::ostringstream oss(std::ostringstream::out);
    oss << *self;
    return oss.str().c_str();
  }
}

// Addition operator with a scalar (left add)
%extend FwdDiff
{
  FwdDiff __add__(T x) const
  {
    return FwdDiff<T>(*$self)+=x;
  }
}

// Addition operator with a scalar (right add)
%extend FwdDiff
{
  FwdDiff __radd__(T x) const
  {
    return FwdDiff<T>(*$self)+=x;
  }
}

// Addition operator with another FwdDiff
%extend FwdDiff
{
  FwdDiff __add__(FwdDiff x) const
  {
    return FwdDiff<T>(*$self)+=x;
  }
}

// Subtraction operator with a scalar (left subtract)
%extend FwdDiff
{
  FwdDiff __sub__(T x) const
  {
    return FwdDiff<T>(*$self)-=x;
  }
}

// Subtraction operator with a scalar (right subtract)
%extend FwdDiff
{
  FwdDiff __rsub__(T x) const
  {
    return -FwdDiff<T>(*$self)+=x;
  }
}

// Subtraction operator with another FwdDiff
%extend FwdDiff
{
  FwdDiff __sub__(FwdDiff x) const
  {
    return FwdDiff<T>(*$self)-=x;
  }
}

// Multiplication operator with a scalar (left multiply)
%extend FwdDiff
{
  FwdDiff __mul__(T x) const
  {
    return FwdDiff<T>(*$self)*=x;
  }
}

// Multiplication operator with a scalar (right multiply)
%extend FwdDiff
{
  FwdDiff __rmul__(T x) const
  {
    return FwdDiff<T>(*$self)*=x;
  }
}

// Multiplication operator with another FwdDiff
%extend FwdDiff
{
  FwdDiff __mul__(FwdDiff x) const
  {
    return FwdDiff<T>(*$self)*=x;
  }
}

// Division operator with a scalar
%extend FwdDiff
{
  FwdDiff __truediv__(T x) const
  {
    return FwdDiff<T>(*$self)/=x;
  }
}

// Division operator with a scalar in reversed order
%extend FwdDiff
{
  FwdDiff __rtruediv__(T x) const
  {
    return FwdDiff<T>(x)/=*$self;
  }
}

// Division operator with another FwdDiff
%extend FwdDiff
{
  FwdDiff __truediv__(FwdDiff x) const
  {
    return FwdDiff<T>(*$self)/=x;
  }
}

// Less than operator
%extend FwdDiff
{
  bool __lt__(FwdDiff x) const
  {
    return (*$self) < x;
  }
}

// Less than or equal to operator
%extend FwdDiff
{
  bool __le__(FwdDiff x) const
  {
    return (*$self) <= x;
  }
}

// Greater than operator
%extend FwdDiff
{
  bool __gt__(FwdDiff x) const
  {
    return (*$self) > x;
  }
}

// Greater than or equal to operator
%extend FwdDiff
{
  bool __ge__(FwdDiff x) const
  {
    return (*$self) >= x;
  }
}

// Instantiate FwdDiff classes
%template(FwdDiff_double) FwdDiff<double>;

// Instantiate inline functions
%template(sin_double) sin<double>;
%template(cos_double) cos<double>;
%template(tan_double) tan<double>;
%template(asin_double) asin<double>;
%template(acos_double) acos<double>;
%template(atan_double) atan<double>;
%template(atan2_double) atan2<double>;
%template(sinh_double) sinh<double>;
%template(cosh_double) cosh<double>;
%template(tanh_double) tanh<double>;
%template(asinh_double) asinh<double>;
%template(acosh_double) acosh<double>;
%template(atanh_double) atanh<double>;
%template(exp_double) exp<double>;
%template(expm1_double) expm1<double>;
%template(log_double) log<double>;
%template(log10_double) log10<double>;
%template(log1p_double) log1p<double>;
%template(pow_double) pow<double>;
%template(sqrt_double) sqrt<double>;
%template(cbrt_double) cbrt<double>;
%template(hypot_double) hypot<double>;
%template(abs_double) abs<double>;
