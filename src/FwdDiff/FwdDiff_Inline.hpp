/*----------------------------------------------------------------------*\
 *  WEdiff Automatic Differentiation Library
 *  Copyright (C) 2017 Christopher T. DeGroot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
\*----------------------------------------------------------------------*/

#ifndef _FWD_DIFF_INLINE_HPP_
#define _FWD_DIFF_INLINE_HPP_

#include <exception>
#include <iostream>

/**
 * @file  FwdDiff_Inline.hpp
 * @brief Contains all inline function overloads for class FwdDiff
 *
 *        Most of the functions in the functions in the C++ math library
 *        are implemented for FwdDiff types, with the exception of the
 *        following:
 *
 *        Exponential and logarithmic:
 *         - frexp, ldexp, modf, exp2, ilogb, log2, logb, scalbn, scalbln
 *
 *        Error and gamma functions:
 *         - erf, erfc, tgamma, lgamma
 *
 *        Rounding and remainder functions:
 *         - All
 *
 *        Floating-point manipulation functions:
 *         - All
 *
 *        Minimum, maximum, difference functions:
 *         - fdim
 *
 *        Other functions:
 *         - fma
 */

/**
 *  @name Addition Operators
 */
///@{

/** Binary addition operator with types FwdDiff and T */
template <typename T>
inline FwdDiff<T> operator+(const T x, FwdDiff<T> y)
{
  return y+=x;
}

/** Binary addition operator with types FwdDifff and T */
template <typename T>
inline FwdDiff<T> operator+(FwdDiff<T> x, const T y)
{
  return x+=y;
}

/** Binary addition operator with types FwdDiff */
template <typename T>
inline FwdDiff<T> operator+(FwdDiff<T> x, const FwdDiff<T>& y)
{
  return x+=y;
}

///@} // Addition Operators

/**
 *  @name Subtraction Operators
 */
///@{

/** Binary subtraction operator with types FwdDiff and T */
template <typename T>
inline FwdDiff<T> operator-(const T x, FwdDiff<T> y)
{
  return -y+=x;
}

/** Binary subtraction operator with types FwdDifff and T */
template <typename T>
inline FwdDiff<T> operator-(FwdDiff<T> x, const T y)
{
  return x-=y;
}

/** Binary subtraction operator with types FwdDiff */
template <typename T>
inline FwdDiff<T> operator-(FwdDiff<T> x, const FwdDiff<T>& y)
{
  return x-=y;
}

///@} // Subtraction Operators

/**
 *  @name Multiplication Operators
 */
///@{

/** Binary multiplication operator with types FwdDiff and T */
template <typename T>
inline FwdDiff<T> operator*(const T x, FwdDiff<T> y)
{
  return y*=x;
}

/** Binary multiplication operator with types FwdDifff and T */
template <typename T>
inline FwdDiff<T> operator*(FwdDiff<T> x, const T y)
{
  return x*=y;
}

/** Binary multiplication operator with types FwdDiff */
template <typename T>
inline FwdDiff<T> operator*(FwdDiff<T> x, const FwdDiff<T>& y)
{
  return x*=y;
}

///@} // Multiplication Operators

/**
 *  @name Division Operators
 */
///@{

/** Binary division operator with types FwdDiff and T */
template <typename T>
inline FwdDiff<T> operator/(const T x, const FwdDiff<T>& y)
{
  return FwdDiff<T>(x)/=y;
}

/** Binary division operator with types FwdDiff and T */
template <typename T>
inline FwdDiff<T> operator/(FwdDiff<T> x, const T y)
{
  return x/=y;
}

/** Binary division operator with types FwdDiff */
template <typename T>
inline FwdDiff<T> operator/(FwdDiff<T> x, const FwdDiff<T>& y)
{
  return x/=y;
}

///@} // Division Operators

/**
 *  @name Comparison Operators
 */
///@{

/** Less than operator */
template <typename T>
inline bool operator<(const FwdDiff<T> x, const FwdDiff<T> y)
{
  return (x.val() < y.val());
}

/** Less than or equal to operator */
template <typename T>
inline bool operator<=(const FwdDiff<T> x, const FwdDiff<T> y)
{
  return (x.val() <= y.val());
}

/** Greater than operator */
template <typename T>
inline bool operator>(const FwdDiff<T> x, const FwdDiff<T> y)
{
  return (x.val() > y.val());
}

/** Greater than or equal to operator */
template <typename T>
inline bool operator>=(const FwdDiff<T> x, const FwdDiff<T> y)
{
  return (x.val() >= y.val());
}

///@} // Comparison Operators

/**
 *  @name Trigonometric Functions
 */
///@{

/** Sine function */
template <typename T>
inline FwdDiff<T> sin(const FwdDiff<T> x)
{
  return FwdDiff<T>(sin(x.val()), cos(x.val())*x.dx());
}

/** Cosine function */
template <typename T>
inline FwdDiff<T> cos(const FwdDiff<T> x)
{
  return FwdDiff<T>(cos(x.val()), -sin(x.val())*x.dx());
}

/** Tangent function */
template <typename T>
inline FwdDiff<T> tan(const FwdDiff<T> x)
{
  return FwdDiff<T>(tan(x.val()), x.dx()/pow(cos(x.val()), 2.0));
}

/** Arcsine function */
template <typename T>
inline FwdDiff<T> asin(const FwdDiff<T> x)
{
  return FwdDiff<T>(asin(x.val()), x.dx()/sqrt(1.0 - pow(x.val(), 2.0)));
}

/** Arccos function */
template <typename T>
inline FwdDiff<T> acos(const FwdDiff<T> x)
{
  return FwdDiff<T>(acos(x.val()), -x.dx()/sqrt(1.0 - pow(x.val(), 2.0)));
}

/** Arctan function */
template <typename T>
inline FwdDiff<T> atan(const FwdDiff<T> x)
{
  return FwdDiff<T>(atan(x.val()), x.dx()/(1.0 + pow(x.val(), 2.0)));
}

/** Arctan function with two parameters */
template <typename T>
inline FwdDiff<T> atan2(const FwdDiff<T> y, const FwdDiff<T> x)
{
  FwdDiff<T> quotient = y/x;
  return FwdDiff<T>(atan2(y.val(), x.val()), quotient.dx()/(1.0 + quotient.val()*quotient.val()));
}

///@} // Trigonometric Functions

/**
 *  @name Hyperbolic Functions
 */
///@{

/** Hyperbolic sine function */
template <typename T>
inline FwdDiff<T> sinh(const FwdDiff<T> x)
{
  return FwdDiff<T>(sinh(x.val()), cosh(x.val())*x.dx());
}

/** Hyperbolic cosine function */
template <typename T>
inline FwdDiff<T> cosh(const FwdDiff<T> x)
{
  return FwdDiff<T>(cosh(x.val()), sinh(x.val())*x.dx());
}

/** Hyperbolic tangent function */
template <typename T>
inline FwdDiff<T> tanh(const FwdDiff<T> x)
{
  return FwdDiff<T>(tanh(x.val()), x.dx()/pow(cosh(x.val()), 2.0));
}

/** Hyperbolic arcsine function */
template <typename T>
inline FwdDiff<T> asinh(const FwdDiff<T> x)
{
  return FwdDiff<T>(asinh(x.val()), x.dx()/sqrt(pow(x.val(), 2.0) + 1.0));
}

/** Hyperbolic arccos function */
template <typename T>
inline FwdDiff<T> acosh(const FwdDiff<T> x)
{
  return FwdDiff<T>(acosh(x.val()), x.dx()/sqrt(pow(x.val(), 2.0) - 1.0));
}

/** Hyperbolic arctan function */
template <typename T>
inline FwdDiff<T> atanh(const FwdDiff<T> x)
{
  return FwdDiff<T>(atanh(x.val()), x.dx()/(1.0 - pow(x.val(), 2.0)));
}

///@} // Hyperbolic Functions

/**
 *  @name Exponential and Logarithmic Functions
 */
///@{

/** Exponential function */
template <typename T>
inline FwdDiff<T> exp(const FwdDiff<T> x)
{
  return FwdDiff<T>(exp(x.val()), exp(x.val())*x.dx());
}

/** Exponential minus one function */
template <typename T>
inline FwdDiff<T> expm1(const FwdDiff<T> x)
{
  return FwdDiff<T>(expm1(x.val()), exp(x.val())*x.dx());
}

/** Natural logarithm function */
template <typename T>
inline FwdDiff<T> log(const FwdDiff<T> x)
{
  return FwdDiff<T>(log(x.val()), x.dx()/x.val());
}

/** Base 10 logarithm function */
template <typename T>
inline FwdDiff<T> log10(const FwdDiff<T> x)
{
  return FwdDiff<T>(log10(x.val()), log10(exp(1.0))/x.val()*x.dx());
}

/** Natural logarithm plus one function */
template <typename T>
inline FwdDiff<T> log1p(const FwdDiff<T> x)
{
  return FwdDiff<T>(log1p(x.val()), x.dx()/(x.val()+1.0));
}

///@} // Exponential and Logarithmic Functions

/**
 *  @name Power Functions
 */
///@{

/** Power function */
template <typename T>
inline FwdDiff<T> pow(const FwdDiff<T> x, const FwdDiff<T> y)
{
  T val = pow(x.val(), y.val());
  return FwdDiff<T>(val,
      y.val()*pow(x.val(), y.val()-1.0)*x.dx() + log(x.val())*val*y.dx());
}

/** Power function (first argument of type T) */
template <typename T>
inline FwdDiff<T> pow(const T x, const FwdDiff<T> y)
{
  return FwdDiff<T>(pow(x, y.val()), log(x)*pow(x, y.val())*y.dx());
}

/** Power function (second argument of type T) */
template <typename T>
inline FwdDiff<T> pow(const FwdDiff<T> x, const T y)
{
  return FwdDiff<T>(pow(x.val(), y), y*pow(x.val(), y-1.0)*x.dx());
}

/** Square root function */
template <typename T>
inline FwdDiff<T> sqrt(const FwdDiff<T> x)
{
  return FwdDiff<T>(sqrt(x.val()), 0.5/sqrt(x.val())*x.dx());
}

/** Cube root function */
template <typename T>
inline FwdDiff<T> cbrt(const FwdDiff<T> x)
{
  return FwdDiff<T>(cbrt(x.val()), pow(x.val(), -2.0/3.0)/3.0*x.dx());
}

/** Hypotenuse function */
template <typename T>
inline FwdDiff<T> hypot(const FwdDiff<T> x, const FwdDiff<T> y)
{
  return sqrt(pow(x, 2.0) + pow(y, 2.0));
}

///@} // Power Functions

/**
 *  @name Minimum, maximum, difference functions
 */
///@{

/** Maximum function */
template <typename T>
inline FwdDiff<T> max(const FwdDiff<T> x, const FwdDiff<T> y)
{
  if (x.val() >= y.val())
  {
    return FwdDiff<T>(x);
  }
  else
  {
    return FwdDiff<T>(y);
  }
}

/** Maximum function */
template <typename T>
inline FwdDiff<T> fmax(const FwdDiff<T> x, const FwdDiff<T> y)
{
  return max(x, y);
}

/** Minimum function */
template <typename T>
inline FwdDiff<T> min(const FwdDiff<T> x, const FwdDiff<T> y)
{
  if (x.val() <= y.val())
  {
    return FwdDiff<T>(x);
  }
  else
  {
    return FwdDiff<T>(y);
  }
}

/** Minimum function */
template <typename T>
inline FwdDiff<T> fmin(const FwdDiff<T> x, const FwdDiff<T> y)
{
  return min(x, y);
}

///@} // Minimum, maximum, difference functions

/**
 *  @name Other Functions
 */
///@{

/** Absolute value function */
template <typename T>
inline FwdDiff<T> abs(const FwdDiff<T> x)
{
  if (x.val() >= 0.0)
  {
    return FwdDiff<T>(x);
  }
  else
  {
    return -FwdDiff<T>(x);
  }
}

/** Absolute value function */
template <typename T>
inline FwdDiff<T> fabs(const FwdDiff<T> x)
{
  return abs(x);
}

///@} // Other Functions

/**
 *  @name Output Operators
 */
///@{

/** Standard output stream */
template <typename T>
inline std::ostream& operator<<(std::ostream& out, const FwdDiff<T> x)
{
  out << "[" << x.val() << " , ";
  size_t last = x.dx().size() - 1;
  for (size_t i = 0; i != last; ++i)
  {
    out << x.dx(0) << " , ";
  }
  out << x.dx(last) << "]";
  return out;
}

///@} // Output Operators

#endif
