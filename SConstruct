import os

# Get the required environment variables
env = Environment(ENV = os.environ)

# Clone the environment to set up the required c++ environment
cppEnv = Environment().Clone()

# Set compiler flags
cppEnv.Append(CXXFLAGS = ['-Wall', '-O3', '-std=c++11'])

# Set path to source files
cppEnv.Append(CPPPATH = [Dir('#src').abspath])

# Set up profiling
cppEnv.Append(LIBS = ['profiler'])

# Clone the c++ environment to set up the SWIG environment
swigEnv = cppEnv.Clone()

# Set up boost paths in c++ environment
try:
    cppEnv.Append(CPPPATH = [env['ENV']['BOOST_INCLUDE_DIR']])
except KeyError:
    cppEnv.Append(CPPPATH = '/usr/local/include/boost')
try:
    cppEnv.Append(LIBPATH = [env['ENV']['BOOST_LIB_DIR']])
except KeyError:
    cppEnv.Append(LIBPATH = '/usr/local/lib')

# Set up Python include directory
swigEnv.Append(CPPPATH = [env['ENV']['PYTHON_INCLUDE_DIR']])

# Set up Python library directory
try:
    swigEnv.Append(LIBPATH = [env['ENV']['PYTHON_LIB_DIR']])
except KeyError:
    swigEnv.Append(LIBPATH = '/usr/local/lib')

# Set up Python version
try:
    pythonVersion = env['ENV']['PYTHON_VERSION']
except KeyError:
    pythonVersion = ''

# Set up include directories for SWIG
for path in swigEnv['CPPPATH']:
	swigEnv.Append(SWIGFLAGS = ['-I%s' % path])

# Set SWIG-specific flags
if Platform().name == 'darwin':
    swigEnv.Append(SHLINKFLAGS = ['-L%s' % env['ENV']['PYTHON_LIB_DIR'], '-lpython%s' % env['ENV']['PYTHON_VERSION'], '-shared'])
else:
    swigEnv.Append(SHLINKFLAGS = ['-lpython%s' % pythonVersion, '-shared'])
swigEnv.Append(SWIGFLAGS = ['-python', '-c++', '-Wall'])
swigEnv['SHLIBPREFIX'] = '_'
swigEnv['SHLIBSUFFIX'] = '.so'

# Get the install directory
try:
    install_dir = env['ENV']['WEDIFF_INST_DIR']
except KeyError:
    install_dir = '/usr/local/'

# Set up aliases to install the library
cppEnv.Alias('install', install_dir)
swigEnv.Alias('install', install_dir)

# Export the environments and install directory variable
Export('cppEnv')
Export('swigEnv')
Export('install_dir')

# Call the SConscript for the c++ sources
cppEnv.SConscript('src/SConscript', variant_dir='build', duplicate=0)
