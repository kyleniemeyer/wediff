{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic Example\n",
    "\n",
    "The following example shows how to import `WEdiff` and its math library into the Python environment, define a function, and evaluate the value and derivative."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from wediff.double import FwdDiff\n",
    "from wediff.double import math\n",
    "\n",
    "def f(x):\n",
    "    \"\"\"Generic mathematical function\"\"\"\n",
    "    return x*math.sin(math.pow(x, 2.0)) + x\n",
    "\n",
    "# Create a FwdDiff number\n",
    "x = FwdDiff(2.0, [1.0])\n",
    "\n",
    "# Evaluate the function\n",
    "y = f(x)\n",
    "\n",
    "# Report the value and derivative\n",
    "print(\"f(x) at x = 2.0: {}\".format(y.val()))\n",
    "print(\"df/dx at x = 2.0: {}\".format(y.dx(0)))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example above, it can be verified that the derivative value is correct by differentiating by hand. The following code computes the exact value of the derivative above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "\n",
    "def dfdx(x):\n",
    "    \"\"\"Analytical derivative of function f(x)\"\"\"\n",
    "    return math.sin(math.pow(x, 2.0)) + x*math.cos(math.pow(x, 2.0))*2.0*x + 1.0;\n",
    "    \n",
    "# Report the exact derivative\n",
    "print(\"df/dx at x = 2.0: {}\".format(dfdx(2.0)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In these examples, only one math library was used at a time. Sometimes, one might need to use both the `FwdDiff` math library as well as the standard library. This can be accomplished by importing them with different names, as shown below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from wediff.double import FwdDiff\n",
    "from wediff.double import math\n",
    "import math as float_math\n",
    "\n",
    "def f(x):\n",
    "    \"\"\"Generic mathematical function\"\"\"\n",
    "    return x*math.sin(math.pow(x, 2.0)) + x\n",
    "\n",
    "def dfdx(x):\n",
    "    \"\"\"Analytical derivative of function f(x)\"\"\"\n",
    "    return float_math.sin(float_math.pow(x, 2.0)) + x*float_math.cos(float_math.pow(x, 2.0))*2.0*x + 1.0;\n",
    "\n",
    "# Create a FwdDiff number\n",
    "x = FwdDiff(2.0, [1.0])\n",
    "\n",
    "# Evaluate the function\n",
    "y = f(x)\n",
    "\n",
    "# Report the value and derivative\n",
    "print(\"f(x) at x = 2.0: {}\".format(y.val()))\n",
    "print(\"df/dx (computed) at x = 2.0: {}\".format(y.dx(0)))\n",
    "print(\"df/dx (exact) at x = 2.0: {}\".format(dfdx(x.val())))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Multivariable Example\n",
    "\n",
    "The `FwdDiff` class is capable of computing the derivative with respect to multiple variables at a time by configuring the objects through the `FwdDiffManager` class. This class follows a monostate pattern such that there is only one instance of the class, since all member data is static. This means that changing the number of derivative components using `setNumComponents`, as shown below, will apply to any `FwdDiff` object that is subsequently created. For performance reasons, the number of components is not checked within the overloaded operators, so the user should be careful to ensure that variables with differnt numbers of derivative components are not mixed.\n",
    "\n",
    "To evaluate derivatives with respect to a certain variable, its derivative component must be initialized to 1. When there are multiple variables, the component that is initialized determines the ordering of the independent variable. Each derivative component should not be initialized more than once. In the example below, `x` is the first variable, since its first derivative component is initialized as 1; `y` is the second variable since its second derivative component is initialized to 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from wediff.double import FwdDiff\n",
    "from wediff.double import FwdDiffManager\n",
    "from wediff.double import math\n",
    "import math as float_math\n",
    "\n",
    "def f(x, y):\n",
    "    \"\"\"Generic mathematical function\"\"\"\n",
    "    return x*y*math.sin(math.pow(x, 2.0)) + x*y*math.cos(y)\n",
    "\n",
    "def dfdx(x, y):\n",
    "    \"\"\"Analytical derivative of function f(x,y) with respect to x\"\"\"\n",
    "    return y*float_math.sin(float_math.pow(x, 2.0)) \\\n",
    "            + x*y*float_math.cos(float_math.pow(x, 2.0))*2.0*x \\\n",
    "            + y*float_math.cos(y);\n",
    "\n",
    "def dfdy(x, y):\n",
    "    \"\"\"Analytical derivative of function f(x,y) with respect to y\"\"\"\n",
    "    return x*float_math.sin(float_math.pow(x, 2.0)) \\\n",
    "            + x*float_math.cos(y) \\\n",
    "            - x*y*float_math.sin(y)\n",
    "\n",
    "# Set up the number of components\n",
    "FwdDiffManager().setNumComponents(2)\n",
    "\n",
    "# Create two FwdDiff numbers (x is first independent variable; y is second independent variable)\n",
    "# The secondary component is initialized to 1 to tag the independent variables\n",
    "x = FwdDiff(2.0, [1.0, 0.0])\n",
    "y = FwdDiff(3.0, [0.0, 1.0])\n",
    "\n",
    "# Evaluate the function\n",
    "z = f(x, y)\n",
    "\n",
    "# Report the value and derivative\n",
    "print(\"f(x,y) at (x, y) = (2.0, 3.0): {}\".format(z.val()))\n",
    "print(\"df/dx (computed) at (x, y) = (2.0, 3.0): {}\".format(z.dx(0)))\n",
    "print(\"df/dx (exact) at (x, y) = (2.0, 3.0): {}\".format(dfdx(x.val(), y.val())))\n",
    "print(\"df/dy (computed) at (x, y) = (2.0, 3.0): {}\".format(z.dx(1)))\n",
    "print(\"df/dy (exact) at (x, y) = (2.0, 3.0): {}\".format(dfdy(x.val(), y.val())))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Before moving to next example, reset the number of components\n",
    "from wediff.double import FwdDiffManager\n",
    "FwdDiffManager().reset()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Solution of Logistic Equation\n",
    "\n",
    "The logistic equation differential equation may be defined as:\n",
    "\n",
    "$$ \\frac{dx}{dt} = r x (1 - x) $$\n",
    "\n",
    "The analytical solution of this ordinary differential equation is :\n",
    "\n",
    "$$ x(t) = \\frac{1}{1 + \\left(\\frac{1}{x_0} - 1\\right) \\exp(-r t)} $$\n",
    "\n",
    "where $x_0$ is the initial value of $x$. Using automatic differntiation we can get the sensitivity of the solution $x(t)$ with respect to the parameter $r$. This can also be computed analytically as:\n",
    "\n",
    "$$ \\frac{\\partial x}{\\partial r} = \\frac{\\left(\\frac{1}{x_0} - 1\\right) t \\exp(-r t)}{\\left[1 + \\left(\\frac{1}{x_0} - 1\\right) \\exp(-r t)\\right]^2} $$\n",
    "\n",
    "In the example below, the ODE is solved using `FwdDiff` numbers and a simple forward Euler integration method, and the solution is compared to the exact values for both the solution and the derivative of the solution with respect to the parameter $r$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from wediff.double import FwdDiff\n",
    "import math\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "def f(t, x0, r):\n",
    "    \"\"\"Function defining analytical solution for x\"\"\"\n",
    "    e = math.exp(-r*t)\n",
    "    return 1/(1 + (1/x0 - 1)*e)\n",
    "\n",
    "def dxdr(t, x0, r):\n",
    "    \"\"\"Function defining analytical solution for dx/dr\"\"\"\n",
    "    e = math.exp(-r*t)\n",
    "    return ((1/x0 - 1)*t*e)/math.pow(1 + (1/x0 - 1)*e, 2)\n",
    "\n",
    "def dxdt(x, r):\n",
    "    \"\"\"Function defining dx/dt (right side of ODE)\"\"\"\n",
    "    return r*x*(1 - x)\n",
    "\n",
    "# Set up time parameters\n",
    "tStart = 0.0\n",
    "tEnd = 10.0\n",
    "dt = FwdDiff(0.01)\n",
    "nTimes = int((tEnd - tStart)/dt.val())\n",
    "times = [dt.val()*i for i in range(nTimes)]\n",
    "\n",
    "# Set initial condition\n",
    "x0 = FwdDiff(0.05)\n",
    "\n",
    "# Set value of parameter 'r' and initialize as independent variable\n",
    "r = FwdDiff(1.0, [1.0])\n",
    "\n",
    "# Create lists to store solutions \n",
    "# Note: dx/dr = 0 at t = 0 since initial condition does not depend on r\n",
    "x_computed = [x0.val()]\n",
    "x_exact = [x0.val()]\n",
    "dx_computed = [0.0]\n",
    "dx_exact = [0.0]\n",
    "\n",
    "# Solve ODE\n",
    "x = x0\n",
    "for t in times[1:]:\n",
    "    x = x + dxdt(x, r)*dt\n",
    "    x_computed.append(x.val())\n",
    "    dx_computed.append(x.dx(0))\n",
    "    x_exact.append(f(t, x0.val(), r.val()))\n",
    "    dx_exact.append(dxdr(t, x0.val(), r.val()))\n",
    "\n",
    "# Plot the results\n",
    "plt.plot(times, x_computed, label=\"x(t) - Computed\", linestyle='-')\n",
    "plt.plot(times, x_exact, label=\"x(t) - Exact\", linestyle='--')\n",
    "plt.plot(times, dx_computed, label=\"dx/dr - Computed\", linestyle='-')\n",
    "plt.plot(times, dx_exact, label=\"dx/dr - Exact\", linestyle='--')\n",
    "plt.xlabel(\"t\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
