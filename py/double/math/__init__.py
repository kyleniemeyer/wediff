"""
  WEdiff Automatic Differentiation Library
  Copyright (C) 2017 Christopher T. DeGroot

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
"""

from wediff.fwd_diff import sin_double as sin
from wediff.fwd_diff import cos_double as cos
from wediff.fwd_diff import tan_double as tan
from wediff.fwd_diff import asin_double as asin
from wediff.fwd_diff import acos_double as acos
from wediff.fwd_diff import atan_double as atan
from wediff.fwd_diff import atan2_double as atan2
from wediff.fwd_diff import sinh_double as sinh
from wediff.fwd_diff import cosh_double as cosh
from wediff.fwd_diff import tanh_double as tanh
from wediff.fwd_diff import asinh_double as asinh
from wediff.fwd_diff import acosh_double as acosh
from wediff.fwd_diff import atanh_double as atanh
from wediff.fwd_diff import exp_double as exp
from wediff.fwd_diff import expm1_double as expm1
from wediff.fwd_diff import log_double as log
from wediff.fwd_diff import log10_double as log10
from wediff.fwd_diff import log1p_double as log1p
from wediff.fwd_diff import pow_double as pow
from wediff.fwd_diff import sqrt_double as sqrt
from wediff.fwd_diff import cbrt_double as cbrt
from wediff.fwd_diff import hypot_double as hypot